import os
import toml

from pathlib import Path
from lxml import etree

from flask import Flask, Response, render_template, request

app = Flask(__name__)

app.config.update(toml.loads(open(os.path.join(Path(__file__).parent.resolve(), 'app.toml')).read()))


@app.route('/Autodiscover/Autodiscover.xml', methods=('GET', 'POST',))
def autodiscover():
    email = None

    if request.method == 'POST':
        parser = etree.fromstring(request.data)

        email = parser.find('.//Request/EMailAddress', namespaces=parser.nsmap).text()

    return Response(
        render_template(
            'autodiscover.xml',
            email=email,
            config=app.config,
        ),
        mimetype='application/xml',
    )


if __name__ == '__main__':
    app.run()
